/*******************************************************************************
* Имя файла              : motor_control.c
*******************************************************************************/

/*******************************************************************************
*
* Включения заголовочных файлов
*
*******************************************************************************/
#include "stm32f10x.h"
#include "motor_control.h"
#include "pwm.h"
#include "gpio.h"


/*******************************************************************************
*
* Макросы
*
*******************************************************************************/


/*******************************************************************************
*
* Описания внешних переменных
*
*******************************************************************************/


/*******************************************************************************
*
* Объявления внешних переменных
*
*******************************************************************************/


/*******************************************************************************
*
* Объявления локальных переменных
*
*******************************************************************************/


/*******************************************************************************
*
* Прототипы внешних функций
*
*******************************************************************************/


/*******************************************************************************
*
* Прототипы локальных функций
*
*******************************************************************************/



/******************************** Программа ***********************************/


/*******************************************************************************
*
* Локальные функции
*
*******************************************************************************/



/*******************************************************************************
*
* Глобальные функции
*
*******************************************************************************/
void MotorControl_Init(void)
{
	GPIO_MODER(DRV_nSLEEP_PORT, DRV_nSLEEP_PIN, OUT_MODE_2MHz);
	GPIO_PUPDR(DRV_nSLEEP_PORT, DRV_nSLEEP_PIN, OUT_PUSHP);

	GPIO_MODER(DRV_nFAULT_PORT, DRV_nFAULT_PIN, INP_MODE);
	GPIO_PUPDR(DRV_nFAULT_PORT, DRV_nFAULT_PIN, INP_FLOATING);

#if defined DRV8701E
	GPIO_MODER(DRV_IN1_PORT, DRV_IN1_PIN, OUT_MODE_2MHz);
	GPIO_PUPDR(DRV_IN1_PORT, DRV_IN1_PIN, OUT_PUSHP);
	SET_GPIO_SET(DRV_IN1_PORT, DRV_IN1_PIN);
#elif defined DRV8701P
	GPIO_MODER(DRV_IN2_PORT, DRV_IN2_PIN, OUT_MODE_2MHz);
	GPIO_PUPDR(DRV_IN2_PORT, DRV_IN2_PIN, OUT_PUSHP);
	SET_GPIO_SET(DRV_IN2_PORT, DRV_IN2_PIN);
#endif

	tCfgPort port;

#if defined DRV8701E
	port.name = DRV_IN2_PORT;
	port.pin = DRV_IN2_PIN;
	Timer_Init(DRV_TIMER, DRV_IN2_TIMER_CH, &port, 1);
#elif defined DRV8701P	
	port.name = DRV_IN1_PORT;
	port.pin = DRV_IN1_PIN;
	Timer_Init(DRV_TIMER, DRV_IN1_TIMER_CH, &port, 1);
#endif

	/* Настройка ШИМ-таймера */
	Timer_PWM_Config(DRV_TIMER, DRV_PWM_FREQ);

#if defined DRV8701E
	Timer_PWM_Setup_Channel(DRV_TIMER, DRV_IN2_TIMER_CH, DRV_IN2_TIMER_CH_MODE);
	Timer_PWM_Set_DutyCycle(DRV_TIMER, DRV_IN2_TIMER_CH, 0);
#elif defined DRV8701P
	Timer_PWM_Setup_Channel(DRV_TIMER, DRV_IN1_TIMER_CH, DRV_IN1_TIMER_CH_MODE);
	Timer_PWM_Set_DutyCycle(DRV_TIMER, DRV_IN1_TIMER_CH, 0);
#endif


	Timer_Start(DRV_TIMER);
}

void MotorControl_setDutyCycle(float duty_cycle)
{
	duty_cycle = 100 - duty_cycle; 
	if (duty_cycle < 0)
		duty_cycle = 0;
	if (duty_cycle > 100)
		duty_cycle = 100;
#if defined DRV8701E
	Timer_PWM_Set_DutyCycle(DRV_TIMER, DRV_IN2_TIMER_CH, duty_cycle);
#elif defined DRV8701P
	Timer_PWM_Set_DutyCycle(DRV_TIMER, DRV_IN1_TIMER_CH, duty_cycle);
#endif
}

void Driver_Enable(void)
{
#if defined DRV8701E
	SET_GPIO_CLR(DRV_IN1_PORT, DRV_IN1_PIN);
#elif defined DRV8701P
	SET_GPIO_SET(DRV_IN2_PORT, DRV_IN2_PIN);
#endif
	SET_GPIO_SET(DRV_nSLEEP_PORT, DRV_nSLEEP_PIN);
}

void Driver_Disable(void)
{
	SET_GPIO_CLR(DRV_nSLEEP_PORT, DRV_nSLEEP_PIN);
#if defined DRV8701E
	SET_GPIO_SET(DRV_IN1_PORT, DRV_IN1_PIN);
#elif defined DRV8701P
	SET_GPIO_SET(DRV_IN2_PORT, DRV_IN2_PIN);
#endif
}

bool Driver_Fault(void)
{
	return !READ_GPIO_PIN(DRV_nFAULT_PORT, DRV_nFAULT_PIN);
}

void Driver_Break(void)
{
	SET_GPIO_SET(DRV_IN2_PORT, DRV_IN2_PIN);
	Timer_PWM_Set_DutyCycle(DRV_TIMER, DRV_IN1_TIMER_CH, 100);
}


/******************************* Конец файла **********************************/
