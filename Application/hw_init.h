#ifndef HW_INIT_H
#define HW_INIT_H


/*******************************************************************************
*
* Включения заголовочных файлов
*
*******************************************************************************/
#include <stdbool.h>
#include "gpio_macros.h"


/*******************************************************************************
*
* Макросы
*
*******************************************************************************/
#define USART_NUM				nUSART1
#define USART_DELAY				4	// задержка перед передачей в мс.

#define RS485_TX_PORT			GPIOB
#define RS485_TX_PIN			6

#define RS485_RX_PORT			GPIOB
#define RS485_RX_PIN			7

#define RS485_DE_PORT			GPIOB
#define RS485_DE_PIN			5

#define RS485_RE_PORT			GPIOB
#define RS485_RE_PIN			4

#define LED_HW_OK_PORT			GPIOA
#define LED_HW_OK_PIN			0

#define LED_USART_TX_PORT		GPIOA
#define LED_USART_TX_PIN		1

#define LED_USART_RX_PORT		GPIOA
#define LED_USART_RX_PIN		2

/*******************************************************************************
*
* Объявления типов
*
*******************************************************************************/


/*******************************************************************************
*
* Описания глобальных переменных
*
*******************************************************************************/


/*******************************************************************************
*
* Прототипы глобальных функций
*
*******************************************************************************/
bool HW_Init(void);
void HW_LED_Output(bool state);


#endif /* HW_INIT_H */
/******************************* Конец файла **********************************/
