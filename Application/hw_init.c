/*******************************************************************************
* Имя файла              : hw_init.c
*******************************************************************************/

/*******************************************************************************
*
* Включения заголовочных файлов
*
*******************************************************************************/
#include "stm32f10x.h"
#include "hw_init.h"
#include "core_cm3.h"
#include "gpio.h"
#include "sysclock.h"
#include "systick.h"
#include "motor_control.h"
#include "pwm.h"
#include "capture.h"
#include "usart.h"


/*******************************************************************************
*
* Макросы
*
*******************************************************************************/


/*******************************************************************************
*
* Описания внешних переменных
*
*******************************************************************************/


/*******************************************************************************
*
* Объявления внешних переменных
*
*******************************************************************************/


/*******************************************************************************
*
* Объявления локальных переменных
*
*******************************************************************************/


/*******************************************************************************
*
* Прототипы внешних функций
*
*******************************************************************************/


/*******************************************************************************
*
* Прототипы локальных функций
*
*******************************************************************************/



/******************************** Программа ***********************************/


/*******************************************************************************
*
* Локальные функции
*
*******************************************************************************/


/*******************************************************************************
*
* Глобальные функции
*
*******************************************************************************/
bool HW_Init(void)
{
	tCfgPort port[2];

//	SysClock_Init(HSE);
	if (SysTick_Init(1000) == false)
		return false;

	GPIO_EnableClock(GPIOA);
	GPIO_EnableClock(GPIOB);
	GPIO_EnableClock(GPIOC);
	GPIO_EnableClock(GPIOD);

	GPIO_AFR(REMAP_PD01, ALT_FUNC_1);	// PD0/1 для кварца
	GPIO_AFR(REMAP_USART1, ALT_FUNC_1);	// PB6/7 для USART1
	GPIO_AFR(REMAP_SWJ, ALT_FUNC_1);	// PB4 отключение SWO

	port[0].name = RS485_TX_PORT;
	port[0].pin = RS485_TX_PIN;	// TX
	port[0].alt_func = ALTF_OUT_PUSHP;
	port[0].mode = OUT_MODE_50MHz;
	port[1].name = RS485_RX_PORT;
	port[1].pin = RS485_RX_PIN;	// RX
	port[1].alt_func = INP_FLOATING;
	port[1].mode = INP_MODE;

	USART_Init(USART_NUM, port, sizeof(port) / sizeof(port[0]));
	USART_Setup(USART_NUM, 9600);

	GPIO_MODER(LED_HW_OK_PORT, LED_HW_OK_PIN, OUT_MODE_2MHz);
	GPIO_PUPDR(LED_HW_OK_PORT, LED_HW_OK_PIN, OUT_ODRAIN);
	GPIO_MODER(LED_USART_TX_PORT, LED_USART_TX_PIN, OUT_MODE_2MHz);
	GPIO_PUPDR(LED_USART_TX_PORT, LED_USART_TX_PIN, OUT_ODRAIN);
	GPIO_MODER(LED_USART_RX_PORT, LED_USART_RX_PIN, OUT_MODE_2MHz);
	GPIO_PUPDR(LED_USART_RX_PORT, LED_USART_RX_PIN, OUT_ODRAIN);
	SET_GPIO_SET(LED_USART_RX_PORT, LED_USART_RX_PIN);
	SET_GPIO_SET(LED_USART_TX_PORT, LED_USART_TX_PIN);

	GPIO_MODER(RS485_DE_PORT, RS485_DE_PIN, OUT_MODE_2MHz);
	GPIO_PUPDR(RS485_RE_PORT, RS485_RE_PIN, OUT_ODRAIN);
	GPIO_PUPDR(RS485_DE_PORT, RS485_DE_PIN, OUT_PUSHP);
	GPIO_MODER(RS485_RE_PORT, RS485_RE_PIN, OUT_MODE_2MHz);
	SET_GPIO_CLR(RS485_RE_PORT, RS485_RE_PIN);	// на прием
	SET_GPIO_CLR(RS485_DE_PORT, RS485_DE_PIN);

	SET_GPIO_CLR(LED_HW_OK_PORT, LED_HW_OK_PIN);

	return true;
}

void HW_LED_Output(bool state)
{
	SET_GPIO(LED_HW_OK_PORT, LED_HW_OK_PIN, !state);
}

/******************************* Конец файла **********************************/
