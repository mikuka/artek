/*******************************************************************************
* Имя файла              : protocol.c
*******************************************************************************/

/*******************************************************************************
*
* Включения заголовочных файлов
*
**********************-*********************************************************/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "protocol.h"
#include "usart.h"
#include "hw_init.h"
#include "systick.h"


/*******************************************************************************
*
* Макросы
*
*******************************************************************************/


/*******************************************************************************
*
* Описания внешних переменных
*
*******************************************************************************/


/*******************************************************************************
*
* Объявления внешних переменных
*
*******************************************************************************/


/*******************************************************************************
*
* Объявления локальных переменных
*
*******************************************************************************/
char devAddr[2] = DEV_ADDR;


/*******************************************************************************
*
* Прототипы внешних функций
*
*******************************************************************************/


/*******************************************************************************
*
* Прототипы локальных функций
*
*******************************************************************************/



/******************************** Программа ***********************************/


/*******************************************************************************
*
* Локальные функции
*
*******************************************************************************/
/*
** reverse string in place 
*/
void reverse(char *s)
{
	char *j;
	int c;

	j = s + strlen(s) - 1;
	while(s < j) {
	c = *s;
	*s++ = *j;
	*j-- = c;
	}
}

char getNumber(int n)
{
	if(n>9)
		return n-10+'A';
	else
		return n+'0';
}

void itoa(int n, char * s, int base_n)
{
	int i, sign;

	if ((sign = n) < 0)  /* record sign */
	n = -n;		/* make n positive */
	i = 0;
	do {	/* generate digits in reverse order */
		s[i++] = getNumber(n % base_n);	/* get next digit */
	} while ((n /= base_n) > 0);	/* delete it */
	if (sign < 0)
	s[i++] = '-';
	s[i] = '\0';
	reverse(s);
}

static void toggleTxLED(bool state)
{
	if (state)
		SET_GPIO_CLR(LED_USART_TX_PORT, LED_USART_TX_PIN);
	else
		SET_GPIO_SET(LED_USART_TX_PORT, LED_USART_TX_PIN);
}

static void toggleRxLED(bool state)
{
	if (state)
		SET_GPIO_CLR(LED_USART_RX_PORT, LED_USART_RX_PIN);
	else
		SET_GPIO_SET(LED_USART_RX_PORT, LED_USART_RX_PIN);
}

static void rs485TxMode(bool state)
{
	if (state) {
		SET_GPIO_SET(RS485_RE_PORT, RS485_RE_PIN);
		SET_GPIO_SET(RS485_DE_PORT, RS485_DE_PIN);
	} else {
		SET_GPIO_CLR(RS485_RE_PORT, RS485_RE_PIN);
		SET_GPIO_CLR(RS485_DE_PORT, RS485_DE_PIN);
	}
}


/*******************************************************************************
*
* Глобальные функции
*
*******************************************************************************/
bool rs485_getPacket(tPacket *rx)
{
	int i;
	char *pc = (char *)rx;

	rs485TxMode(false);

	for(i = 0; i < sizeof(tPacket); i++) {
		if (USART_getchar(USART_NUM, &pc[i]) == false)
			goto err;
		toggleRxLED(true);
	}

	if (rx->hdr != PROTOCOL_HDR || rx->end != PROTOCOL_END)
		goto err;

	i = 0;
	while(i < sizeof(rx->addr)) {
		if (devAddr[i] != rx->addr[i])
			goto err;
		i++;
	}
	toggleRxLED(false);
	return true;

err:
	toggleRxLED(false);
	return false;
}

bool rs485_sendPacket(tPacket *tx)
{
	char *pc = &tx->hdr;
	tx->hdr = PROTOCOL_HDRANS;
	tx->end = PROTOCOL_END;

	rs485TxMode(true);
	DelayMs(USART_DELAY);
    volatile char d;
	for(int i = 0; i < sizeof(tPacket); i++) {
		d = pc[i];
		if (USART_putchar(USART_NUM, pc[i]) == false)
			goto err;

		toggleTxLED(true);
	}
	toggleTxLED(false);
	rs485TxMode(false);
	return true;

err:
	toggleTxLED(false);
	rs485TxMode(false);
	return false;
}

uint32_t rs485_parseData(tPacket *rx)
{
	char str[sizeof(rx->data) + 1];

	memcpy(str, rx->data, sizeof(rx->data));
	str[sizeof(rx->data)] = '\0';

	return (uint32_t)strtol(str, NULL, 16);
}



void rs485_fillData(tPacket *tx, uint32_t data)
{
	char str[3];
	size_t len;
	char *p;

	itoa(data, str, 16);

	len = strlen(str);
	p = &str[0];
	memset(tx->data, '0', sizeof(tx->data));
	for (size_t i = sizeof(tx->data) - len; i < sizeof(tx->data); i++) {
		tx->data[i] = *p++;
	}
}


/******************************* Конец файла **********************************/
