#ifndef MOTOR_CONTROL_H
#define MOTOR_CONTROL_H


/*******************************************************************************
*
* Включения заголовочных файлов
*
*******************************************************************************/
#include "gpio_macros.h"
#include <stdbool.h>


/*******************************************************************************
*
* Макросы
*
*******************************************************************************/

/* Частота 10 кГц, реальная частота на выходе - 10.4902 кГц, при 
 * PWM_PERIOD = 1000
 */
#define DRV_PWM_FREQ							70000	// Частота ШИМ 	Гц

#define DRV_IN1_PORT							GPIOA
#define DRV_IN1_PIN								8
#define DRV_TIMER								T1
#define DRV_IN1_TIMER_CH						tCH1
#define DRV_IN1_TIMER_CH_MODE					mNOT_INVERTED

#define DRV_IN2_PORT							GPIOA
#define DRV_IN2_PIN								9
#define DRV_IN2_PORT_AF							ALT_FUNC_3
#define DRV_IN2_TIMER_CH						tCH2
#define DRV_IN2_TIMER_CH_MODE					mNOT_INVERTED

#define DRV_nSLEEP_PORT							GPIOA
#define DRV_nSLEEP_PIN							10

#define DRV_nFAULT_PORT							GPIOA
#define DRV_nFAULT_PIN							12

/*******************************************************************************
*
* Объявления типов
*
*******************************************************************************/


/*******************************************************************************
*
* Описания глобальных переменных
*
*******************************************************************************/


/*******************************************************************************
*
* Прототипы глобальных функций
*
*******************************************************************************/
void MotorControl_Init(void);
void Driver_Enable(void);
void Driver_Disable(void);
void MotorControl_setDutyCycle(float duty_cycle);
bool Driver_Fault(void);
void Driver_Break(void);


#endif /* MOTOR_CONTROL_H */
/******************************* Конец файла **********************************/
