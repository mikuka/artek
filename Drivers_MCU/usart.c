#include "stm32f10x.h"
#include "usart.h"
#include "gpio.h"
#include "gpio_macros.h"
#include "sysclock.h"
#include "systick.h"

#define NUSART			2


struct USART
{
	USART_TypeDef *baseAddr;
	const uint32_t apb_clk;
};

struct USART usart[NUSART] = {
	{
		USART1,
		Peripheral2Clock,
	},
	{
		USART2,
		Peripheral1Clock,
	},
};

void USART1_IRQHandler()
{
	NVIC_ClearPendingIRQ( USART1_IRQn );
}

void USART2_IRQHandler()
{
	NVIC_ClearPendingIRQ( USART2_IRQn );
}

void USART_Init(tUSART bus, tCfgPort *cfgport, size_t cfgport_len)
{
	switch( bus )
	{
	case nUSART1:
		RCC->APB2ENR |= RCC_APB2ENR_USART1EN;
		break;
	case nUSART2:
		RCC->APB1ENR |= RCC_APB1ENR_USART2EN;
		break;
	}
	
	while(cfgport_len) {
		if (cfgport) {
			GPIO_EnableClock(cfgport->name);
			GPIO_MODER(cfgport->name, cfgport->pin, cfgport->mode);
			GPIO_PUPDR(cfgport->name, cfgport->pin, cfgport->alt_func);
		}
		++cfgport;
		--cfgport_len;
	};
}

void USART_Setup(tUSART bus, uint32_t baudrate)
{
	USART_TypeDef *USARTx = (&usart[bus])->baseAddr;

	uint16_t integerdivider = (uint16_t)( usart[bus].apb_clk / baudrate * 100 / 
							8 / (2 - 0));
	uint16_t fractionaldivider = ((integerdivider % 100) * 16) / 100;
	USARTx->BRR = (integerdivider / 100 << 4) | (fractionaldivider & 0x0f);
	USARTx->CR1 |= USART_CR1_TE | USART_CR1_RE | USART_CR1_UE;
}

bool USART_putchar(tUSART bus, char c)
{
	USART_TypeDef *USARTx = (&usart[bus])->baseAddr;
	volatile uint32_t timeout = 10000;

	USART1->DR = c;
	while( !( USARTx->SR & USART_SR_TC ) ){
		if (timeout-- <= 0)
			return false;
	}

	return true;
}

bool USART_getchar(tUSART bus, char *c)
{
	USART_TypeDef *USARTx = (&usart[bus])->baseAddr;
	volatile uint32_t timeout = 10000;

	while( !( USARTx->SR & USART_SR_RXNE ) ){
		if (timeout-- <= 0)
			return false;
	}
	*c = USART1->DR & 0xff;

	return true;
}
  
