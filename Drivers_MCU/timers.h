#ifndef TIMERS_H
#define TIMERS_H


/*******************************************************************************
*
* Включения заголовочных файлов
*
*******************************************************************************/
#include <stdbool.h>
#include <stdlib.h>
#include "gpio.h"


/*******************************************************************************
*
* Макросы
*
*******************************************************************************/
#define NTIMERS				5


/*******************************************************************************
*
* Объявления типов
*
*******************************************************************************/
/* Имена таймеров */
typedef enum
{
	T1 = 0,
	T2,
	T3,
	T6,
	T7,
}tTimer;

/* Имена каналов от 1 до 4 */
typedef enum 
{
	tCH1 = 0,
	tCH2,
	tCH3,
	tCH4,
}tTimerChx;

/* Тип входа/выхода */
typedef enum
{
	mNOT_INVERTED,	//< Прямой
	mINVERTED,		//< Инверсный
}tChMode;

/* Структура с ресурсами таймеров */
typedef struct
{
	TIM_TypeDef *baseAddr;
	char ntimer;
	char nchannel_mask;
	// uint32_t freqHz;
	IRQn_Type	nirq;
}tResTimer;


/*******************************************************************************
*
* Описания глобальных переменных
*
*******************************************************************************/


/*******************************************************************************
*
* Прототипы глобальных функций
*
*******************************************************************************/
void Timer_Init(tTimer timer, tTimerChx channel, \
				const tCfgPort *cfgport, size_t len);
tResTimer *Timer_getResources(tTimer timer);
void Timer_Start(tTimer timer);
void Timer_Stop(tTimer timer);
void Timer_clearEventFlag(tTimer timer);
bool Timer_getEventFlag(tTimer timer);
void Timer_registerLowLevelHandler(tTimer timer, void (*handler)(void *), void **arg);
void Timer_registerHighLevelHandler(tTimer timer, void (*handler)(void *), void **arg);


#endif /* TIMERS_H */
/******************************* Конец файла **********************************/
