#ifndef CAPTURE_H
#define CAPTURE_H


/*******************************************************************************
*
* Включения заголовочных файлов
*
*******************************************************************************/
#include "timers.h"


/*******************************************************************************
*
* Макросы
*
*******************************************************************************/


/*******************************************************************************
*
* Объявления типов
*
*******************************************************************************/


/*******************************************************************************
*
* Описания глобальных переменных
*
*******************************************************************************/


/*******************************************************************************
*
* Прототипы глобальных функций
*
*******************************************************************************/
void Timer_Capture_Config(tTimer timer, uint32_t freqHz);
void Timer_Capture_Setup_Channel(tTimer timer, tTimerChx channel, tChMode mode,
				uint8_t filter_step);
uint32_t Timer_Capture_getCounter(tTimer timer, tTimerChx channel);


void Timer_Capture_Start(tTimer timer);
uint32_t Timer_Capture_getTimeout(tTimer timer);


#endif /* CAPTURE_H */
/******************************* Конец файла **********************************/
