/*******************************************************************************
* Имя файла              : capture.c
*******************************************************************************/

/*******************************************************************************
*
* Включения заголовочных файлов
*
*******************************************************************************/
#include "stm32f10x.h"
#include "timers.h"



/*******************************************************************************
*
* Макросы
*
*******************************************************************************/


/*******************************************************************************
*
* Описания внешних переменных
*
*******************************************************************************/


/*******************************************************************************
*
* Объявления внешних переменных
*
*******************************************************************************/


/*******************************************************************************
*
* Объявления локальных переменных
*
*******************************************************************************/

enum tLocalName {TIMER1, TIMER2, TIMER3, TIMER6, TIMER7};

tResTimer Timers[NTIMERS] = {
	{   
		TIM1,
		0,
		0,
		TIM1_UP_TIM16_IRQn,
	},
	{   
		TIM2,
		0,
		0,
		TIM2_IRQn,
	},
	{
		TIM3,
		0,
		0,
		TIM3_IRQn,
	},
	{
		TIM6,
		0,
		0,
		TIM6_DAC_IRQn,
	},
	{
		TIM7,
		0,
		0,
		TIM7_IRQn,
	},
};

void (*plowl_handler[NTIMERS])(void *) = {0, 0, 0, 0, 0};
void **plowl_argument[NTIMERS] = {0, 0, 0, 0, 0,};
void (*phighl_handler[NTIMERS])(void *) = {0, 0, 0, 0, 0};
void **phighl_argument[NTIMERS] = {0, 0, 0, 0, 0,};

static volatile bool eventFlagIsr[NTIMERS] = {0, 0, 0, 0, 0};

/*******************************************************************************
*
* Прототипы внешних функций
*
*******************************************************************************/


/*******************************************************************************
*
* Прототипы локальных функций
*
*******************************************************************************/



/******************************** Программа ***********************************/


/*******************************************************************************
*
* Локальные функции
*
*******************************************************************************/

/*
 * Установка флага возникновения прерывания из обработчика прерывания
 */
static inline void setEventFlagFromISR(const uint8_t ntimer)
{
	eventFlagIsr[ntimer] = true;	
}

/*
 * Низкоуровневый обработчик
 */
inline static void common_handler(const uint8_t ntimer)
{
	setEventFlagFromISR(ntimer);
	if (plowl_handler[ntimer])
		plowl_handler[ntimer](plowl_argument[ntimer]);
	if (phighl_handler[ntimer])
		phighl_handler[ntimer](phighl_argument[ntimer]);
}

void TIM1_IRQHandler(void)
{
	TIM1->SR &= ~TIM_SR_UIF;   // Сброс флага события изменения состояни таймера
	common_handler(TIMER1);
}

void TIM2_IRQHandler(void)
{
	TIM2->SR &= ~TIM_SR_UIF;   // Сброс флага события изменения состояни таймера
	common_handler(TIMER2);
}

void TIM3_IRQHandler(void)
{
	TIM3->SR &= ~TIM_SR_UIF;   // Сброс флага события изменения состояни таймера
	common_handler(TIMER3);
}

void TIM6_DAC_IRQHandler(void)
{

	TIM6->SR &= ~TIM_SR_UIF;   // Сброс флага события изменения состояни таймера
	common_handler(TIMER6);
}

void TIM7_IRQHandler(void)
{
	TIM7->SR &= ~TIM_SR_UIF;   // Сброс флага события изменения состояни таймера
	common_handler(TIMER7);
}

/*******************************************************************************
*
* Глобальные функции
*
*******************************************************************************/
void Timer_Init(tTimer timer, tTimerChx channel, \
				const tCfgPort *cfgport, size_t len)
{
	switch(timer){
	case T1:
		RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
		break;
	case T2:
		RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
		break;
	case T3:
		RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
		break;
	case T6:
		RCC->APB1ENR |= RCC_APB1ENR_TIM6EN;
		break;
	case T7:
		RCC->APB1ENR |= RCC_APB1ENR_TIM7EN;
		break;
	default:
		return;
	}
	
	GPIO_EnableClock(cfgport->name);

	while(len) {
		GPIO_MODER(cfgport->name, cfgport->pin, OUT_MODE_50MHz);
		GPIO_PUPDR(cfgport->name, cfgport->pin, ALTF_OUT_PUSHP);
		++cfgport;
		--len;
	}

	Timers[timer].nchannel_mask |= (1 << (channel));
}


tResTimer *Timer_getResources(tTimer timer)
{
	return &Timers[timer];
}

void Timer_Start(tTimer timer)
{
	tResTimer *ptimer = Timer_getResources(timer);
	TIM_TypeDef *TIMx = ptimer->baseAddr;
	
	TIMx->CR1 |= TIM_CR1_CEN; //Запуск счета
}

void Timer_Stop(tTimer timer)
{
	tResTimer *ptimer = Timer_getResources(timer);
	TIM_TypeDef *TIMx = ptimer->baseAddr;
	
	TIMx->CR1 &= ~TIM_CR1_CEN;
}

/*
 * Публичный метод для сброса флага возникновения прерывания
 */
void Timer_Counter_clearEventFlag(tTimer timer)
{
	eventFlagIsr[timer] = false;
}

/*
 * Публичный метод для считывания состояния флага возникновения прерывания
 */
bool Timer_Counter_getEventFlag(tTimer timer)
{
 	return eventFlagIsr[timer];
}

/*
 * Регистрация колбэка, вызываемого при возникновении прерывания
 */
void Timer_registerLowLevelHandler(tTimer timer, void (*handler)(void *), void **arg)
{
	if (handler)
		plowl_handler[timer] = handler;
	if (arg)
		plowl_argument[timer] = arg;
}

/*
 * Регистрация колбэка, вызываемого при возникновении прерывания
 */
void Timer_registerHighLevelHandler(tTimer timer, void (*handler)(void *), void **arg)
{
	if (handler)
		phighl_handler[timer] = handler;
	if (arg)
		phighl_argument[timer] = arg;
}

/******************************* Конец файла **********************************/
