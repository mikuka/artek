/*************************************************************************
* ��� �����              : I2C.cpp
*
*
* ��������               : ������� ���� I2C
*
*************************************************************************/

#include <stdio.h>
#include "stm32f10x.h"
#include "gpio_macros.h"


CI2CBus::CI2CBus( I2C::mode mode, I2C::name name )
{
	switch( name )
	{
	case I2C::I2C_1:
		// ���������� ���� B � �������� �������
		RCC->AHBENR |= RCC_AHBENR_GPIOBEN;  

		GPIO_MODER(I2C1_SCL_PORT, I2C1_SCL_PIN, ALT_FUNC_MODE);
		GPIO_MODER(I2C1_SDA_PORT, I2C1_SDA_PIN, ALT_FUNC_MODE);

		GPIO_OTYPER(I2C1_SCL_PORT, I2C1_SCL_PIN, OPENDRAIN_TYPE);
		GPIO_OTYPER(I2C1_SDA_PORT, I2C1_SDA_PIN, OPENDRAIN_TYPE);

		GPIO_AFR(I2C1_SCL_PORT, I2C1_SCL_PIN, ALT_FUNC_1);
		GPIO_AFR(I2C1_SDA_PORT, I2C1_SDA_PIN, ALT_FUNC_1);

		RCC->APB1ENR |= RCC_APB1ENR_I2C1EN;
		RCC->CFGR3 |= RCC_CFGR3_I2C1SW;   // �������� ��������� �������� ��� �����
		I2Cn = I2C1;
		break;

	case I2C::I2C_2:
		RCC->APB1ENR |= RCC_APB1ENR_I2C2EN;

		I2Cn = I2C2;
		break;

	default:
		return;
	}
		
	reinit();
}


CI2CBus& CI2CBus::getInstanceN1()
{
	static CI2CBus i2cbus_object;

	return i2cbus_object;
}


void CI2CBus::reinit()
{
  	I2C1->CR1 &= ~0x00CFE0FF;

	// ��������� ��������� �� 100 ��� ����� �� User Manual, ���. 542 (Revision 4)
	I2Cn->TIMINGR = ( 0x0b << 28 ) | ( 0x04 << 20 ) | ( 0x02 << 16 ) |
	( 0x0f << 8 ) | ( 0x13 << 0 );

	// ��������� ��������� �� 10 ��� ����� �� User Manual, ���. 542 (Revision 4)
	// *( baseAddr + I2C_TIMINGR_OFFS ) = ( 0x0b << 28 ) | ( 0x0f << 20 ) | ( 0x0f << 16 ) |
	//                                    ( 0xc3 << 8 ) | ( 0xc7 << 0 );    

	I2Cn->CR1 |= I2C_CR1_PE; 
}



/*************************************************************************
* ��� �������            : send
*
* ���������              : addr    - ����� ���������� ������ �� ���� I2C
*                          *buff   - ��������� �� ����� � ������������� �������
*                          size    - ����� ������������ ����
*
* ������������ ��������  : ��. ���� RetVals.hpp
*
* ��������               : �������� ��������� �������� ������ ������������������ ����.
*                          �������� ���������� � ������� START, ����� ���������� ������,
*                          � �������� ������������ �������� STOP
*
*************************************************************************/
TRetVal CI2CBus::send( uint8_t addr, const uint8_t* buff, uint8_t size )
{
	timeout = I2C_TIMEOUT;
	while( --timeout )
	{
		if( !( I2Cn->ISR & I2C_ISR_BUSY ) )
		{
			break;
		}
	}

	if( !timeout )
	{
		reinit();
		return rvTIME_OUT;
	}

	I2Cn->CR2 = addr;       // ��������� ����� ������
	I2Cn->CR2 |= ( size << 16 ); // ������� ���� ����� ���������
	I2Cn->CR2 &= ~I2C_CR2_RD_WRN;     // ����������� �������� - ������
	I2Cn->CR2 |= I2C_CR2_NACK;
	I2Cn->CR2 |= I2C_CR2_START | I2C_CR2_AUTOEND;
	
	if( I2Cn->ISR & I2C_ISR_NACKF )
	{
		return rvI2C_SLAVE_NOT_FOUND;
	}

	while( size-- )
	{
		timeout = I2C_TIMEOUT;
		while( !( I2Cn->ISR & I2C_ISR_TXE ) )
		{
			timeout--;
			if( !timeout )
			{
				reinit();
				return rvTIME_OUT;
			} 
		}	  

		I2Cn->TXDR = *buff++;
	}

	return rvOK;
}



/*************************************************************************
* ��� �������            : read
*
* ���������              : addr    - ����� ���������� ������ �� ���� I2C
*                          *buff   - ��������� �� ����� ���� ����� ����������� ������
*                          size    - ����� ����������� ����
*
* ������������ ��������  : ��. ���� RetVals.hpp
*
* ��������               : �������� ��������� ������� �� ������ ��������� ����������
*							����������.
*
*************************************************************************/
TRetVal CI2CBus::read( uint8_t addr, uint8_t* buff, uint8_t size )
{
	I2Cn->ICR |= I2C_ICR_NACKCF | I2C_ICR_STOPCF;
	timeout = I2C_TIMEOUT;
	while( --timeout )
	{
		if( !( I2Cn->ISR & I2C_ISR_BUSY ) )
		{
			break;
		}
	}

	if( !timeout )
	{
		reinit();
		return rvTIME_OUT;
	} 

	I2Cn->CR2 = addr | I2C_CR2_RD_WRN | ( size << 16 ) | I2C_CR2_START | I2C_CR2_AUTOEND;    

	if( I2Cn->ISR & I2C_ISR_NACKF )
	{
		return rvI2C_SLAVE_NOT_FOUND;
	}

	while( size-- )
	{
		timeout = I2C_TIMEOUT;
		while( !( I2Cn->ISR & I2C_ISR_RXNE ) )
		{
			timeout--;
			if( !timeout )
			{
				reinit();
				return rvTIME_OUT;
			} 
		}

		*buff++ = I2Cn->RXDR;	
	}

	return rvOK;
}



/*************************************************************************
* ��� �������            : start
*
* ���������              : addr	- ����� ������
*
* ������������ ��������  : ��. ���� RetVals.hpp
*
* ��������               : �������� ��������� ������� START �� ����.
*
*************************************************************************/
TRetVal CI2CBus::start( uint8_t addr, uint8_t size )
{
	slaveAddr = addr;
	sizeOfBuff = size;
	I2Cn->ICR |= I2C_ICR_NACKCF | I2C_ICR_STOPCF;

	timeout = I2C_TIMEOUT;
	while( ( I2Cn->ISR & I2C_ISR_BUSY ) )
	{
		timeout--;

		if( !timeout )
		{
			reinit();
			return rvTIME_OUT;
		}
	}

	I2Cn->CR2 = addr | I2C_CR2_START | ( size << 16 );

	if( I2Cn->ISR & I2C_ISR_NACKF )
	{
		return rvI2C_SLAVE_NOT_FOUND;
	}	

	return rvOK;
}



/*************************************************************************
* ��� �������            : writeBuff
*
* ���������              : *buff   - ��������� �� ������������ �����
*                          size    - ����� ������������ ����
*
* ������������ ��������  : ��. ���� RetVals.hpp
*
* ��������               : �������� ��������� �������� ����� ������. �������
*							START � STOP �� �����������!!! �� ���������� ����������
*							���������� ���������. ������� ����� ��������� ��������
*							��� �������������� ������ �� ����.
*
*************************************************************************/
TRetVal CI2CBus::writeBuff( const uint8_t* buff )
{	
	while( sizeOfBuff-- )
	{
		timeout = I2C_TIMEOUT;
		while( !( I2Cn->ISR & I2C_ISR_TXIS ) )
		{
			timeout--;
			if( !timeout )
			{
				reinit();
				return rvTIME_OUT;
			} 
		}

		I2Cn->TXDR = *buff++;
	}

	timeout = I2C_TIMEOUT;
	while( !( I2Cn->ISR & I2C_ISR_TC ) )
	{
		timeout--;
		if( !timeout )
		{
			reinit();
			return rvTIME_OUT;
		} 		
	}

	return rvOK;
}



/*************************************************************************
* ��� �������            : restart
*
* ���������              : ���
*
* ������������ ��������  : ��. ���� RetVals.hpp
*
* ��������               : �������� ��������� ������� RESTART �� ����.
*
*************************************************************************/
TRetVal CI2CBus::restart( uint8_t size )
{
	I2Cn->ICR |= I2C_ICR_NACKCF | I2C_ICR_STOPCF;

	I2Cn->CR2 = slaveAddr | I2C_CR2_RD_WRN | I2C_CR2_START | ( size << 16 );   

	sizeOfBuff = size;	

	if( I2Cn->ISR & I2C_ISR_NACKF )
	{
		return rvI2C_SLAVE_NOT_FOUND;
	}	

	return rvOK;
}



/*************************************************************************
* ��� �������            : readBuff
*
* ���������              : addr    - ����� ���������� ������ �� ���� I2C
*                          *buff   - ��������� �� ����������� �����
*                          size    - ����� ����������� ����
*
* ������������ ��������  : ��. ���� RetVals.hpp
*
* ��������               : �������� ��������� ������� ����� ������ �� ������.
*							��� ���� ������� START � STOP �� ���� �� �����������!!!
*
*************************************************************************/
TRetVal CI2CBus::readBuff( uint8_t* buff )
{
	while( sizeOfBuff-- )
	{
	timeout = I2C_TIMEOUT;
	while( !( I2Cn->ISR & I2C_ISR_RXNE ) )
	{
		timeout--;
		if( !timeout )
		{
			reinit();
			return rvTIME_OUT;				
		}
	}

	*buff++ = I2Cn->RXDR;	

	}

	return rvOK;
}



/*************************************************************************
* ��� �������            : stop
*
* ���������              : ���
*
* ������������ ��������  : ��. ���� RetVals.hpp
*
* ��������               : �������� ��������� ������� STOP �� ����.
*
*************************************************************************/
TRetVal CI2CBus::stop()
{
	I2Cn->CR2 |= I2C_CR2_STOP;
	return rvOK;
}


