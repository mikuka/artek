#ifndef I2C_HPP
#define I2C_HPP

#include "stm32f0xx.h"
#include <inttypes.h>
#include "global_config.hpp"
#include "RetVals.hpp"

#define I2C1_SDA_PORT               GPIOB
#define I2C1_SDA_PIN                7
#define I2C1_SCL_PORT				GPIOB
#define I2C1_SCL_PIN                6

#ifdef __cplusplus
namespace I2C
{
	enum mode
	{
		MASTER,
		SLAVE
	};

	enum name
	{
		I2C_1,
		I2C_2
	};
}


class CI2CBus
{
public:
	// ����� �������� ��� I2C (����������� ����������������)
	static const int I2C_TIMEOUT                   = 100000;

private:
	I2C_TypeDef *I2Cn;   // ������� �����
	volatile int timeout;
	uint8_t slaveAddr;
	uint8_t sizeOfBuff;
	CI2CBus (I2C::mode mode = I2C::MASTER, I2C::name name = I2C::I2C_1);

public:  
	static CI2CBus& getInstanceN1();
	void reinit();

	TRetVal send( uint8_t addr, const uint8_t* buff, uint8_t size );
	TRetVal read( uint8_t addr, uint8_t* buff, uint8_t size );

	TRetVal start( uint8_t addr, uint8_t size );
	TRetVal writeBuff( const uint8_t* buff );
	TRetVal restart( uint8_t size );
	TRetVal readBuff( uint8_t* buff );
	TRetVal stop();
};
  
#endif // __cplusplus
  
  
#endif
