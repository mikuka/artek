/*******************************************************************************
* Имя файла              : counter.c
*******************************************************************************/

/*******************************************************************************
*
* Включения заголовочных файлов
*
*******************************************************************************/
#include <stdbool.h>
#include "stm32f10x.h"
#include "timers.h"
#include "capture.h"



/*******************************************************************************
*
* Макросы
*
*******************************************************************************/
//#define OFFSET(timer)		(timer - T2)


/*******************************************************************************
*
* Описания внешних переменных
*
*******************************************************************************/


/*******************************************************************************
*
* Объявления внешних переменных
*
*******************************************************************************/


/*******************************************************************************
*
* Объявления локальных переменных
*
*******************************************************************************/

volatile uint32_t counterSecIsr[NTIMERS] = {0, 0, 0, 0};
volatile uint32_t *pcounterSecIsr[NTIMERS] = {0, 0, 0, 0};
static uint32_t maxCountSec[NTIMERS] = {0, 0, 0, 0};
static uint32_t timeout[NTIMERS] = {0, 0, 0, 0};

static void (*phandler[NTIMERS])(void *);

/*******************************************************************************
*
* Прототипы внешних функций
*
*******************************************************************************/


/*******************************************************************************
*
* Прототипы локальных функций
*
*******************************************************************************/


/******************************** Программа ***********************************/


/*******************************************************************************
*
* Локальные функции
*
*******************************************************************************/
static void init_counter_mode(TIM_TypeDef *TIMx, uint16_t quantumTimeMs)
{
	TIMx->PSC = (uint32_t)SystemCoreClock / 1000;	// Минимальный квант - 1 мс.
	TIMx->ARR = quantumTimeMs;
//	TIMx->CR1 |= TIM_CR1_URS;// Генерерирование события только по переполнению
	TIMx->CR1 &= ~TIM_CR1_URS;
}

/*
 * Низкоуровневый обработчик
 */
inline static void common_handler(int index)
{
	if (pcounterSecIsr[index]) {
		(*pcounterSecIsr[index])++;
		if (*pcounterSecIsr[index] < maxCountSec[index] )
			return;
		else
			*pcounterSecIsr[index] = 0;
	}
}

static void IsrT1(void *arg)
{
	common_handler(T1);
}

static void IsrT2(void *arg)
{
	common_handler(T2);
}

static void IsrT3(void *arg)
{
	common_handler(T3);
}

static void IsrT6(void *arg)
{
	common_handler(T6);
}

static void IsrT7(void *arg)
{
	common_handler(T7);
}


static void (*phandler[NTIMERS])(void *) = {
	IsrT1,
	IsrT2,
	IsrT3,
	IsrT6,
	IsrT7,
};

/*******************************************************************************
*
* Глобальные функции
*
*******************************************************************************/

/*
 * Установка таймера
 * quantumTimeMs - период таймера в мс.
 */
void Timer_Counter_setupMSec(tTimer timer, uint32_t quantumTimeMs)
{
	tResTimer *res = Timer_getResources(timer);
	TIM_TypeDef *TIMx = res->baseAddr;

	timeout[timer] = quantumTimeMs;
	if (quantumTimeMs > UINT16_MAX) {
		pcounterSecIsr[timer] = &counterSecIsr[timer];
		maxCountSec[timer] = quantumTimeMs - UINT16_MAX;
		quantumTimeMs = 1000;
	} else {
		pcounterSecIsr[timer] = 0;
	}
	
	init_counter_mode(res->baseAddr, quantumTimeMs);

	TIMx->DIER |= TIM_DIER_UIE;
	Timer_registerLowLevelHandler(timer, phandler[timer], 0);
	NVIC_EnableIRQ(res->nirq);
	
	TIMx->EGR |= TIM_EGR_UG;	// Обновить счетчик
}
 
/*
 * Установка таймера
 * quantumTimeS - период таймера в с.
 */
void Timer_Counter_setupSec(tTimer timer, uint16_t quantumTimeS)
{
	Timer_Counter_setupMSec(timer, (uint32_t)quantumTimeS + UINT16_MAX);
}

/*
 * Запуск таймера.
 */
void Timer_Counter_start(tTimer timer)
{
	TIM_TypeDef *TIMx = Timer_getResources(timer)->baseAddr;
	TIMx->CR1 |= TIM_CR1_CEN;
}

/*
 * Останов таймера
 */
void Timer_Counter_stop(tTimer timer)
{	
	TIM_TypeDef *TIMx = Timer_getResources(timer)->baseAddr;

	TIMx->CR1 &= ~TIM_CR1_CEN;
	counterSecIsr[timer] = 0;
	maxCountSec[timer] = 0;
}

/*
 * Возвращает состояние таймера: запущен/остановлен
 */
bool Timer_Counter_isStarted(tTimer timer)
{
	TIM_TypeDef *TIMx = Timer_getResources(timer)->baseAddr;
	return TIMx->CR1 & TIM_CR1_CEN;
}

/*
 * Возвращает значение таймаута таймера
 */
uint32_t Timer_Counter_getTimeout(tTimer timer)
{
	return (timeout[timer] > UINT16_MAX) ? \
		   (timeout[timer] - UINT16_MAX) : \
			timeout[timer];
}

/******************************* Конец файла **********************************/
