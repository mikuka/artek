#ifndef USART_H
#define USART_H

#include <stdlib.h>
#include <stdbool.h>
#include "gpio.h"

typedef enum {
	nUSART1,
	nUSART2,
}tUSART;

/*
 * Сначала идет настройка порта TX, затем RX
 */
void USART_Init(tUSART bus, tCfgPort *cfgport, size_t cfgport_len);
void USART_Setup(tUSART bus, uint32_t baudrate);
bool USART_putchar(tUSART bus, char c);
bool USART_getchar(tUSART bus, char *c);

#endif // USART_H
