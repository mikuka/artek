#include "gpio_macros.h"
#include "stm32f10x.h"
#include "pwm.h"


extern int debug(const char *format, ...);

uint32_t pwmFreq[NTIMERS];


void Timer_PWM_Config(tTimer timer, uint32_t freqHz)
{
	TIM_TypeDef *TIMx = Timer_getResources(timer)->baseAddr;
	
	pwmFreq[timer] = freqHz;
	// Настраиваем прескалер
	TIMx->PSC = ((uint32_t)SystemCoreClock / 1) / PWM_PERIOD / freqHz - 1;
	// Настраиваем период таймера
	/*
	 * Для удобства задания скважности в % в регистре ARR примем период = 100
	 */
	TIMx->ARR = PWM_PERIOD;
}

void Timer_PWM_Setup_Channel(tTimer timer, tTimerChx channel, tChMode mode)
{
	tResTimer *ptimer = Timer_getResources(timer);
	TIM_TypeDef *TIMx = ptimer->baseAddr;
		
	uint32_t *pccmr = (uint32_t *)&TIMx->CCMR1;
	uint32_t *pccr = (uint32_t *)&TIMx->CCR1;

	pccr[channel] = (PWM_PERIOD) & 0xFFFF;
	// Канал в режим выхода сравнения
	pccmr[channel >> 1] &= ~( 0x03 << (8 * (channel % 2)) );
	// Выход ШИМ в режиме сравнения
	pccmr[channel >> 1] |= ( 0x70 << (8 * (channel % 2)) );
	switch (mode) {
	case mNOT_INVERTED:
		// Разрешение выхода
		TIMx->CCER |= 0x01 << (4 * channel);
		break;
	case mINVERTED:
		TIMx->CCER |= 0x04 << (4 * channel);
		break;
	}
	// Только для TIM1/TIM8
	TIMx->BDTR |= TIM_BDTR_MOE;
	// timer->DIER |= TIM_DIER_UIE; //Разрешаем прерывание при переполнении счетчика
	// TIMx->CR1 |= TIM_CR1_CEN; //Запуск счета
}

void Timer_PWM_Set_DutyCycle(tTimer timer, tTimerChx channel, float duty_cycle)
{
	tResTimer *ptimer = Timer_getResources(timer);
	TIM_TypeDef *TIMx = ptimer->baseAddr;
	uint32_t *pccr = (uint32_t *)&TIMx->CCR1;
	// Округление
	uint32_t value = (uint32_t)(duty_cycle * PWM_PERIOD) / 100;
//	debug("duty_cycle %f, val %d, psc %d\r\n", duty_cycle, value, TIMx->PSC);
	pccr[channel] = (PWM_PERIOD - value) & 0xFFFF;
}



