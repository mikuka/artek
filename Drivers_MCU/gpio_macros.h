#ifndef IO_MACROS_H
#define IO_MACROS_H

#include "stm32f10x.h"

#define GPIOX(port)						port

// GPIOx_MODE function
#define INP_MODE						0
#define OUT_MODE_10MHz					1
#define OUT_MODE_2MHz					2
#define OUT_MODE_50MHz					3

// GPIO port configuration
#define REG_GPIO_MODE(port)			( GPIOX(port)->CR )
#define GPIO_MODER(port, pin, mode) 	\
			MODIFICATION_REGISTER(REG_GPIO_MODE(port)[pin >> 0x03], \
			(pin & 0x07), 4, 0x03, mode)

// GPIO port pull-up/pull-down
#define INP_ANALOG_MODE					0
#define INP_FLOATING					1
#define INP_PUP_PDOWN					2
#define OUT_PUSHP						0
#define OUT_ODRAIN						1
#define ALTF_OUT_PUSHP					2
#define ALTF_OUT_ODRAIN					3
#define GPIO_PUPDR(port, pin, cfg) \
			MODIFICATION_REGISTER(REG_GPIO_MODE(port)[pin >> 0x03], \
			(pin & 0x07), 4, 0x0c, cfg << 2)

		
/*#define	MODIFICATION_REGISTER(reg, bit, offset, mask, value) \
			do{ reg &= ~(mask << (bit * offset)); \
			reg |= (value << (bit * offset)); }while(0) */

// GPIO alternate function
#define ALT_FUNC_0						0
#define ALT_FUNC_1						1
#define ALT_FUNC_2						2
#define ALT_FUNC_3						3
#define ALT_FUNC_4						4
#define ALT_FUNC_5						5
#define ALT_FUNC_6						6
#define ALT_FUNC_7						7

#define REMAP_USART1					(2)
#define REMAP_PD01						(15)
#define REMAP_SWJ						(24)

#define REG_GPIO_AFR1					( AFIO->MAPR )
#define REG_GPIO_AFR2					( AFIO->MAPR2 )
#define GPIO_AFR(remap, altfunc)	do{ REG_GPIO_AFR1 |= (altfunc << remap);\
							}while(0)

// GPIO port bit set
#define REG_GPIO_SET(port)				( GPIOX(port)->BSRR )
#define SET_GPIO_SET(port, pin)			( REG_GPIO_SET(port) |= (1 << pin) )

// GPIO port bit reset
#define REG_GPIO_CLR(port)				( GPIOX(port)->BSRR )
#define SET_GPIO_CLR(port, pin)			( REG_GPIO_CLR(port) |= ((1 << pin) << 16) )

// Set Value Port
//#define	LOW									(0)
//#define	HIGH								(1)
#define SET_GPIO(port, pin, value)		( (value) ? \
			( SET_GPIO_SET(port, pin) ) : \
			( SET_GPIO_CLR(port, pin) ) )

// Atomic Set Value Port
#define REG_GPIO_ODR(port)				( GPIOX(port)->ODR )
#define SET_GPIO_ATOMIC(port, pin, value) \
			WRITE_BIT(REG_GPIO_ODR(port), pin, value)

// Read Value Port
#define REG_GPIO_PIN(port)				( GPIOX(port)->IDR )
#define READ_GPIO_PIN(port, pin)		( REG_GPIO_PIN(port) & (1 << pin) )
#define READ_GPIO_PORT(port)			( REG_GPIO_PIN(port) & 0xffff )



#define	MODIFICATION_REGISTER(reg, bit, offset, mask, value) \
			do{ reg &= ~(mask << (bit * offset)); \
			reg |= (value << (bit * offset)); }while(0)

#define	WRITE_BIT(reg, bit, value)			( value ? \
			( reg |= (1 << bit) ) : \
			( reg &= ~(1 << bit) ) )

#endif
