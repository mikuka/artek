#ifndef _PWM_H_
#define _PWM_H_

#include <inttypes.h>
#include "stm32f10x.h"
#include "gpio.h"
#include "timers.h"

/*
 * Вес 1 кванта регистра сравнения таймера.
 * 1 квант = SystemCoreClock / PWM_PERIOD / freqHz
 */
#define PWM_PERIOD			100
/*
 * 1 квант скважности измеренный = deltat = 56нс.
 * 1 квант скважности расчетный = 60нс.
 * период ШИМ = T = 1/10000 = 0.0001
 * 
 */

void Timer_PWM_Config(tTimer timer, uint32_t freqHz);
void Timer_PWM_Setup_Channel(tTimer timer, tTimerChx channel, tChMode mode);
void Timer_PWM_Set_DutyCycle(tTimer timer, tTimerChx channel, float duty_cycle);




#endif // _PWM_H_
