/*******************************************************************************
* Имя файла              : capture.c
*******************************************************************************/

/*******************************************************************************
*
* Включения заголовочных файлов
*
*******************************************************************************/
#include "stm32f10x.h"
#include "timers.h"
#include "capture.h"

#include "gpio_macros.h"


/*******************************************************************************
*
* Макросы
*
*******************************************************************************/


/*******************************************************************************
*
* Описания внешних переменных
*
*******************************************************************************/


/*******************************************************************************
*
* Объявления внешних переменных
*
*******************************************************************************/


/*******************************************************************************
*
* Объявления локальных переменных
*
*******************************************************************************/
static uint32_t timeout[NTIMERS] = {0, 0, 0, 0, 0};

static void (*phandler[NTIMERS])(void *);
static volatile uint32_t value_t3ccr3;


/*******************************************************************************
*
* Прототипы внешних функций
*
*******************************************************************************/
extern int debug(const char *format, ...);


/*******************************************************************************
*
* Прототипы локальных функций
*
*******************************************************************************/

/*
 * Низкоуровневый обработчик
 */
inline static void common_handler(int index)
{

}

static void IsrT1(void *arg)
{
	common_handler(T1);
}

static void IsrT2(void *arg)
{
	common_handler(T2);
}

/*
 * Обработчик оборотов
 */
static void IsrT3(void *arg)
{
	TIM3->SR &= ~TIM_SR_CC3IF;
	common_handler(T3);

	value_t3ccr3 = TIM3->CCR3;
	TIM3->CNT = 0x0000;
}

static void IsrT6(void *arg)
{
	common_handler(T6);
}

static void IsrT7(void *arg)
{
	common_handler(T7);
}


static void (*phandler[NTIMERS])(void *) = {
	IsrT1,
	IsrT2,
	IsrT3,
	IsrT6,
	IsrT7,
};


/******************************** Программа ***********************************/


/*******************************************************************************
*
* Локальные функции
*
*******************************************************************************/
//#define DRV_IN2_PORT							GPIOC
//#define DRV_IN2_PIN								6
/*void TIM3_IRQHandler(void)
{
	volatile static int cnt = 0;
	TIM3->SR &= ~TIM_SR_UIF;   // Сброс флага события изменения состояния таймера
	if (flag)
		SET_GPIO_SET(GPIOC, 8);	// Интервал 2.62 с.
	else
		SET_GPIO_CLR(GPIOC, 8);
	flag = !flag;*/
	//debug("irq %d, %d\r\n", cnt++, TIM3->CCR3);
//}

void handler(void *dummy)
{
	static int flag;
	TIM3->SR &= ~(TIM_SR_UIF | TIM_SR_CC3IF);
	if (flag)
		SET_GPIO_SET(GPIOC, 8);	// Интервал 2.62 с.
	else
		SET_GPIO_CLR(GPIOC, 8);
	flag = !flag;
//	debug("!");
}

/*******************************************************************************
*
* Глобальные функции
*
*******************************************************************************/
void Timer_Capture_Config(tTimer timer, uint32_t quantumTimeUs)
{
	TIM_TypeDef *TIMx = Timer_getResources(timer)->baseAddr;

	TIMx->PSC = ((uint32_t)SystemCoreClock / 2 / 1000000) /* 1us */ * quantumTimeUs;	// Минимальный квант - 1 мкс.
	timeout[timer] = quantumTimeUs;
//	TIMx->ARR = quantumTimeUs;
//	TIMx->ARR = quantumTimeUs;
//	TIMx->CR1 |= TIM_CR1_URS;// Генерерирование события только по переполнению
	TIMx->CR1 &= ~TIM_CR1_URS;	
}

void Timer_Capture_Setup_Channel(tTimer timer, tTimerChx channel, tChMode mode,
				uint8_t filter_step)
{
	TIM_TypeDef *TIMx = Timer_getResources(timer)->baseAddr;
	uint32_t *pccmr1 = (uint32_t *)&TIMx->CCMR1;
	
	// Канал в режиме захвата
	pccmr1[channel >> 1] &= ~( 0x03 << (8 * (channel % 2)) );
	pccmr1[channel >> 1] |= ( 0x01 << (8 * (channel % 2)) );
	// Запись фильтра
	pccmr1[channel >> 1] &= ~( 0xF0 << (8 * (channel % 2)) );
	pccmr1[channel >> 1] |= ( (filter_step << 4) << (8 * (channel % 2)) );
	// Запись предделителя по кол-ву событий, необходимых для изменения счетчика
	pccmr1[channel >> 1] &= ~( 0x0C << (8 * (channel % 2)) );	// Без деления
	// Сброс настройки фронта захвата и инверсии входа
	TIMx->CCER &= ~(0x0A << (4 * channel));
	// Разрешение входа
	TIMx->CCER |= 0x01 << (4 * channel);

	switch (mode) {
	case mNOT_INVERTED:
		
		break;
	case mINVERTED:

		break;
	}
	uint32_t *pccr = (uint32_t *)&TIMx->CCR1;
	pccr[channel] = 0x00;
}

void Timer_Capture_Start(tTimer timer)
{
	tResTimer *ptimer = Timer_getResources(timer);
	TIM_TypeDef *TIMx = ptimer->baseAddr;
	
//	TIMx->CR1 |= TIM_CR1_CEN; //Запуск счета
	
	TIMx->DIER |= /*TIM_DIER_UIE | */	TIM_DIER_CC3IE;

//	GPIO_MODER(GPIOC, 8, OUT_FUNC_MODE);	// удалить
	
	Timer_registerHighLevelHandler(timer, phandler[timer], 0);
	NVIC_EnableIRQ(ptimer->nirq);

	TIMx->CR1 |= TIM_CR1_CEN;
	TIMx->EGR |= TIM_EGR_UG;     // Обновить счетчик
}

/*uint32_t Timer_Capture_getCounter(tTimer timer, tTimerChx channel)
{
	TIM_TypeDef *TIMx = Timer_getResources(timer)->baseAddr;
	uint32_t *pccr = (uint32_t *)&TIMx->CCR1;

	return pccr[channel];
}*/

uint32_t Timer_Capture_getCounter(tTimer timer, tTimerChx channel)
{
	return value_t3ccr3;
}

/*
 * Возвращает значение таймаута таймера
 */
uint32_t Timer_Capture_getTimeout(tTimer timer)
{
	return timeout[timer];
}

/******************************* Конец файла **********************************/
