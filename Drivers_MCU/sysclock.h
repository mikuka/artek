#ifndef SYSCLOCK_H
#define SYSCLOCK_H

#define Peripheral1Clock		24000000UL	// (SystemCoreClock / 4)
#define Peripheral2Clock		24000000UL	// (SystemCoreClock / 2)

typedef enum
{
	HSI,
	HSE,
}tHS_XT;

int SysClock_Init(tHS_XT source_clock);


#endif // SYSCLOCK_H
