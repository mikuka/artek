/***************************************************************************//**
*
* @file                 systick.h
* @brief                
* @version      
* @date                 15 февр. 2014 г.
* @author               Kuchuk Mikhail mailto:kuchuk@dev2lab.com
*
*******************************************************************************/


#ifndef SYSTICK_H_
#define SYSTICK_H_


/*******************************************************************************
*
* Включения заголовочных файлов
*
*******************************************************************************/
#include <stdbool.h>


/*******************************************************************************
*
* Макросы
*
*******************************************************************************/


/*******************************************************************************
*
* Объявления типов
*
*******************************************************************************/


/*******************************************************************************
*
* Описания глобальных переменных
*
*******************************************************************************/


/*******************************************************************************
*
* Прототипы глобальных функций
*
*******************************************************************************/

/**
 * Задержка в мс.
 * @param nTime - значение задержки в мс.
 */
void DelayMs(uint32_t nTime);

/**
 * Загрузка значения в переменную TimingDelay.
 * @param  nTime - значение в мс.
 */
void TimingDelay_LoadTime(uint32_t nTime);

/**
 * Инициализация системного таймера.
 * @param FreqHz - частота тиков в Гц.
 * @return true, если частота задана корректно.
 */
bool SysTick_Init (uint32_t FreqHz);

/**
 * Получение текущего значения системного счетчика
 */
uint32_t GetSysTickCounter(void);

/**
 * Получение значения обновления счетчика в мс.
 */
uint32_t GetSysTickTimeout(void);


#endif /* SYSTICK_H_ */
/******************************* Конец файла **********************************/
